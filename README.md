# PiGamePad

For using a gamepad on a Raspberry Pi. This package is designed to be simple to use, but not super powerful. It also works on other Linux machines.

All pads should work, only the SNES and N64 controllers have button definitions so far.

##

To install run:

    pip install git+https://gitlab.com/bath_open_instrumentation_group/pigamepad