import c_extension_module
import time
import threading
 
class FuncThread(threading.Thread):
    def __init__(self, callback, N):
        self._callback = callback
        self._N = N
        threading.Thread.__init__(self)
 
    def run(self):
        c_extension_module.spawn_non_python_thread(self._callback, self.N)

 



def python_callback(tid,val):
    print("python callback called %d" % tid)
    def python_thread():
        print("python thread started %d %d" %(tid,val))
        print("python thread ended %d" % tid)
    # `threading` might implicitly call PyEval_InitThreads()
    try:
        import threading
    except ImportError:
        print("threadless python %d" % tid)
    else:
        threading.Thread(target=python_thread).start()


t1 = FuncThread(python_callback,4)
t1.start()
time.sleep(4)
print("exit main thread")